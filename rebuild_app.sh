#!/bin/bash
if [[ -d "app" ]]; then
	rm -rf app
fi

if [[ -d "pgdata" ]]; then
	rm -rf pgdata
fi

if [[ -d "state" ]]; then
	rm -rf state
fi

docker-compose up --force-recreate --build