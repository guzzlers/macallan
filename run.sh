unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     machine=Linux;;
    Darwin*)    machine=Mac;;
    CYGWIN*)    machine=Cygwin;;
    MINGW*)     machine=MinGw;;
    *)          machine="UNKNOWN:${unameOut}"
esac

if [ $machine == "Mac" ]; then
	export POSTGRES_USER=tarash
	export POSTGRES_PASSWORD=abc123
	export POSTGRES_PORT=5432
	export DATABASE_NAME=cart_app
	yarn install --check-files
	cd app && rails s
elif [ $machine == "Linux" ]; then
	cd app && bundle
	curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
	echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
	apt-get update && apt-get install yarn
	yarn install --check-files
fi