# Macallan

Contibution Guidelines:
1. First create an issue in Gitlab for the task being done. Gitlab gives issue ID's like Issue#1 or Issue#2 and so on...
2. Always checkout new branch name as Issue#X, for example branch name might be Issue#1 or Issue#2 and like that. This will help in debugging if someone messes up.
3. Avoid use of force push or if at all used, should be done very carefully.
4. When commiting changes for a Gitlab issue(for eg. Issue#69) make sure all commits in the issue branch(name should be Issue#69) have Issue#69 prefixed in every commit message. This will again help in debugging if someone messes up.  

Mac Installation Steps:

1. Follow https://docs.docker.com/docker-for-mac/install/ for Docker Hub installation.
2. Start Docker app from Applications
3. From macallan directory, run sh rebuild_app.sh

NB: Step 3 is taking huge amout of time (~15-20 min). Tarash working on removing bloatware which comes as part of the solidus e-commerce gem.


